# vendor_xiaomi_venus-firmware

Firmware images for Redmi Note 11 (spes), to include in custom ROM builds.

**Current version**: fw_spes_miui_SPESGlobal_V13.0.12.0.RGCMIXM_7142be5717_11.0

### How to use?

1. Clone this repo to `vendor/xiaomi/spes-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/spes-firmware/BoardConfigVendor.mk
```

